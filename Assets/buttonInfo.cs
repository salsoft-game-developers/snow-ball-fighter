﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonInfo : MonoBehaviour
{
    public int ItemID;
    public Text PirceTxt;
    public Text QuantityTxt;
    public GameObject ShopManager;

    void Update()
    {
        PirceTxt.text = "Price: $" + ShopManagerScript.Instance.shopItems[2, ItemID].ToString();
        QuantityTxt.text = ShopManagerScript.Instance.shopItems[3, ItemID].ToString();
    }
}
