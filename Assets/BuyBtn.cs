﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyBtn : MonoBehaviour
{
    public enum ItemType
    {
        Gold50,
        Gold100,
        Gold150,
        NoAds
    }

    public ItemType itemType;
    public Text priceText;

    private string defaultText;

    // Start is called before the first frame update
    void Start()
    {
        defaultText = priceText.text;
        StartCoroutine(LoadPriceRoutine());
        
    }


    public void ClickBuy()
    {
        switch (itemType)
        {
            case ItemType.Gold50:
                IAPManager.Instanace.Buy50Gold();
                break;
            case ItemType.Gold100:
                IAPManager.Instanace.Buy100Gold();
                break;
            case ItemType.Gold150:
                IAPManager.Instanace.Buy150Gold();
                break;
        }
    }
   
    
    private IEnumerator LoadPriceRoutine()
    {
        while (!IAPManager.Instanace.IsInitialized())
            yield return null;

        string loadedPrice = "";
       
        switch (itemType)
        {
            case ItemType.Gold50:
             loadedPrice=   IAPManager.Instanace.GetProducePriceFromStrore(IAPManager.Instanace.Gold_50);
                break;
          
            case ItemType.Gold100:
                loadedPrice = IAPManager.Instanace.GetProducePriceFromStrore(IAPManager.Instanace.Gold_100);

                break;

            case ItemType.Gold150:
                loadedPrice = IAPManager.Instanace.GetProducePriceFromStrore(IAPManager.Instanace.Gold_150);

                break;
        }
        priceText.text = defaultText + "" + loadedPrice;
    }

}
