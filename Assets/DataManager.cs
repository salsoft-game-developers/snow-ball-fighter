﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : Singleton<DataManager>
{
   // public Text goIdAmountText;
  //  public GameObject noAdsButton;
    private int goldAmount = 0;
    private bool noAds = false;

    public void AddGold(int amount)
    {
        goldAmount += amount;
        // goIdAmountText.text = goldAmount.ToString();
        Debug.Log("Gold Amount  :  " + goldAmount);
    }

    public void RemoveAds()
    {
        noAds = true;
       // noAdsButton.SetActive(false);
    }
}
