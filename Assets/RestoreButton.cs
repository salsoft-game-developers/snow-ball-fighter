﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // If we are running on an Apple device ... 
        if (Application.platform != RuntimePlatform.IPhonePlayer &&
            Application.platform != RuntimePlatform.OSXPlayer)
        {
            gameObject.SetActive(false);
        }
    }

    public void ClickRestore()
    {
        IAPManager.Instanace.RestorePurchases();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
