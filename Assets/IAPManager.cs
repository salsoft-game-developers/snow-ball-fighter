﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System;
using UnityEngine.UI;

public class IAPManager : Singleton<IAPManager> , IStoreListener
{
    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
   
        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        public  string Gold_50 = "santa_gold_50";
        public  string Gold_100 = "santa_gold_100";
        public  string Gold_150 = "santa_gold_150";
   
       
    //public  string NO_Ads = "NO_Ads";
  
        void Start()
        {
            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }
        }

        public void InitializePurchasing()
        {
            // If we have already connected to Purchasing ...
            if (IsInitialized())
            {
                // ... we are done here.
                return;
            }

            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            // Add a product to sell / restore by way of its identifier, associating the general identifier
            // with its store-specific identifiers.
            builder.AddProduct(Gold_50, ProductType.Consumable);
            builder.AddProduct(Gold_100, ProductType.Consumable);
            builder.AddProduct(Gold_150, ProductType.Consumable);
            // Continue adding the non-consumable product.
           // builder.AddProduct(NO_Ads, ProductType.NonConsumable);
            

            // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
            // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
            UnityPurchasing.Initialize(this, builder);
        }


        public bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }


        public void Buy50Gold()
        { 
            BuyProductID(Gold_50);
        GlobleVariable.instance.Coins += 150;
        UIManager.instance.txtCashAmount.text = "Cash :" + GlobleVariable.instance.Cash;
        PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);
        UIManager.instance.CashAnimation.SetActive(true);

        UIManager.instance.CashAnimation.GetComponent<Text>().text = "+ 150 CASH";
        StartCoroutine(AnimationObjFalse());

        }
        public void Buy100Gold()
        {            
            BuyProductID(Gold_100);
        GlobleVariable.instance.Coins += 250;
        UIManager.instance.txtCashAmount.text = "Cash :" + GlobleVariable.instance.Cash;
        PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);
        UIManager.instance.CashAnimation.SetActive(true);
        UIManager.instance.CashAnimation.GetComponent<Text>().text = "+ 250 CASH";
        StartCoroutine(AnimationObjFalse());
    }
   
    public void Buy150Gold()
        {            
            BuyProductID(Gold_150);
        GlobleVariable.instance.Cash += 500;
        UIManager.instance.txtCashAmount.text = "Cash :" + GlobleVariable.instance.Cash;
        UIManager.instance.CashAnimation.SetActive(true);
        PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);
        UIManager.instance.CashAnimation.GetComponent<Text>().text = "+ 500 CASH";
        StartCoroutine(AnimationObjFalse());
    }

    //public void BuyNoAds()
    //{    
    //    BuyProductID(NO_Ads);
    //}

    public string GetProducePriceFromStrore(String id)
    {
        if (m_StoreController != null && m_StoreController.products != null)
            return m_StoreController.products.WithID(id).metadata.localizedPriceString;
        else
            return "";

    }
       

        void BuyProductID(string productId)
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }


        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) => {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }


        //  
        // --- IStoreListener
        //

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            Debug.Log("OnInitialized: PASS");

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;
        }


        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }


        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            // A consumable product has been purchased by this user.
            if (String.Equals(args.purchasedProduct.definition.id, Gold_50, StringComparison.Ordinal))
            {
            // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            //TODO
            DataManager.Instanace.AddGold(50);
            }
            
            else if (String.Equals(args.purchasedProduct.definition.id, Gold_100, StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
            DataManager.Instanace.AddGold(100);
            } 
            else if (String.Equals(args.purchasedProduct.definition.id, Gold_150, StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
            DataManager.Instanace.AddGold(150);
            }
        
        //// Or ... a subscription product has been purchased by this user.
        //else if (String.Equals(args.purchasedProduct.definition.id, NO_Ads, StringComparison.Ordinal))
        //    {
        //        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //        // TODO: The subscription item has been successfully purchased, grant this to the player.
        //    }
        //    // Or ... an unknown product has been purchased by this user. Fill in additional products here....
            else
            {
                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            }

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }


        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }

    IEnumerator AnimationObjFalse()
    {
        yield return new WaitForSeconds(1);
        UIManager.instance.CashAnimation.SetActive(false);
    }
    
}
