﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSystem : MonoBehaviour
{
    public int Spell1Price;
    public int Spell2Price;
    public int Spell3Price;
    public GameObject NotEnoughMoneyAnim;

    // Start is called before the first frame update
    void Start()
    {
      GlobleVariable.instance.Cash = PlayerPrefs.GetInt("CashAmount", GlobleVariable.instance.Cash);
        Debug.Log("New Cash is : " + GlobleVariable.instance.Cash);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void GetFreezSpell()
    {
        if(GlobleVariable.instance.Cash >= Spell1Price)
        {
            SetCoin(Spell1Price);
            Debug.Log("get 1");
            GameManager.instance.freezSpellCount += 1;
            UIManager.instance.SmallFreezTxt.text = GameManager.instance.freezSpellCount.ToString();
            PlayerPrefs.SetInt("FreezingTime5", GameManager.instance.freezSpellCount);

            
           GameManager.instance.FreezingSpellBtn.GetComponent<Button>().interactable = true;       // freez Btn Not interactable
            UIManager.instance.ShopCashTxt.text = "Cash: $ " + GlobleVariable.instance.Cash;





        }
        else
        {
            Debug.Log("Dont Have Money 1");
            NotEnoughMoneyAnim.SetActive(true);
            StartCoroutine(NotEnoughWating());

        }
    }
    
    public void GetTimeSpell()
    {
        if (GlobleVariable.instance.Cash >= Spell2Price)
        {
            SetCoin(Spell2Price);
            Debug.Log("GET 2");
            GameManager.instance.TimeSpellCount += 1;
            UIManager.instance.AddTimeTxt.text = GameManager.instance.TimeSpellCount.ToString();
            PlayerPrefs.SetInt("AddTime", GameManager.instance.TimeSpellCount);
            UIManager.instance.ShopCashTxt.text = "Cash: $ " + GlobleVariable.instance.Cash;

        }
        else
        {
            Debug.Log("Dont Have Money 2");
            NotEnoughMoneyAnim.SetActive(true);
            StartCoroutine(NotEnoughWating());

        }
    }  
    public void GetMoreFreezpell()
    {
        if (GlobleVariable.instance.Cash >= Spell3Price)
        {
            SetCoin(Spell3Price);
            Debug.Log("gET 3");
            GameManager.instance.BigFreezTimeSpellCount += 1;
            UIManager.instance.BigFreezTxt.text = GameManager.instance.BigFreezTimeSpellCount.ToString();
            PlayerPrefs.SetInt("FreezingTime5", GameManager.instance.BigFreezTimeSpellCount);
            GameManager.instance.BigFreezingSpell.GetComponent<Button>().interactable = true;       // freez Btn Not interactable
            UIManager.instance.ShopCashTxt.text = "Cash: $ " + GlobleVariable.instance.Cash;


        }
        else
        {
            Debug.Log("Dont Have Money 3");
            NotEnoughMoneyAnim.SetActive(true);
            StartCoroutine(NotEnoughWating());
        }
    }
    void SetCoin(int price) // We are reducing coins after buying
    {
        GlobleVariable.instance.Cash -= price;
       // UIManager.instance.txtCoinAmount.text = "Coin : " + GlobleVariable.instance.Coins;
        PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);
    }

    IEnumerator  NotEnoughWating()
    {
        yield return new WaitForSeconds(1.4f);
        NotEnoughMoneyAnim.SetActive(false);
    }
}
