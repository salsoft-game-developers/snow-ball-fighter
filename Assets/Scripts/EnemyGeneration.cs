﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyGeneration : MonoBehaviour
{
   

    public GameObject EnemyPrefab;
    public GameObject[] spawnPoints;
 
    public List<GameObject> holdEnimiesRef;
    public GameObject enimiesParentObj;
    
    public  int enemeyCount = 0;
   
  //  public float EnemiesHealthMultiplier = 0;

   [HideInInspector] public GameObject enemyClone;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("enemeyCount is : "+ enemeyCount);
   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public void GenerateEnemy()
    {
        int RandNum = Random.Range(0, spawnPoints.Length);

        enemyClone = Instantiate(EnemyPrefab,spawnPoints[RandNum].transform.position, spawnPoints[RandNum].transform.rotation);
        enemyClone.transform.parent = enimiesParentObj.transform;
        holdEnimiesRef.Add(enemyClone);
      
        enemeyCount++;
       // StartCoroutine(WaitingTime());
      //  enemyClone.GetComponent<EnemyAiTutorial>().Enemyhealth += EnemiesHealthMultiplier;
        
        Debug.Log("enemeyCount is : "+enemeyCount);

        if (enemeyCount <= GlobleVariable.instance.EnemeyRange)
        {
            Debug.Log("enemeyCount is Greater");
            StartCoroutine(WaitingTime());
        }
    }

  public  IEnumerator WaitingTime()
    {
        yield return new WaitForSeconds(1f);
        GenerateEnemy();
    }

    public void ResetGenerateEnemy()
    {
        int RandNum = Random.Range(0, spawnPoints.Length);

        enemyClone = Instantiate(EnemyPrefab, spawnPoints[RandNum].transform.position, spawnPoints[RandNum].transform.rotation);
        enemyClone.transform.parent = enimiesParentObj.transform;
        holdEnimiesRef.Add(enemyClone);

      
        // StartCoroutine(WaitingTime());
      

        Debug.Log("enemeyCount is : " + enemeyCount);

        if (enemeyCount <= GlobleVariable.instance.EnemeyRange)
        {
            Debug.Log("enemeyCount is Greater");
            StartCoroutine(ResetGenerationWaitingTime());
        }
    }
    public IEnumerator ResetGenerationWaitingTime()
    {
        yield return new WaitForSeconds(.8f);
        GenerateEnemy();
    }
}
