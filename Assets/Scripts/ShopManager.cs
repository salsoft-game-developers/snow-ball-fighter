﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    #region Singlton:ShopManager

    public static ShopManager Instance;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    #endregion

    [System.Serializable]
    public class ShopItem
    {
        public  string Name = "";
        public int  Price;
        public bool IsPurchased = false;
        public GameObject selectButton;
        public GameObject LockImage;
    }
  
    public  GameObject[] AllCharacter;
    
    public List<ShopItem> ShopItemsList;
    public List<GameObject> priceTemplate;
    public CharacterMovement characterMovement;
    int isPurchasedSkin_0, isPurchasedSkin_1, isPurchasedSkin_2; //We will check whether it has been purchased or not. 



    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("isPurchasedSkin_1", 1);
        for (int i = 0; i < AllCharacter.Length; i++)
        {
            AllCharacter[i].SetActive(false);
        }
        AllCharacter[0].SetActive(true); // set chacter 1 by index
      //  IsItPurchesed();//The function we check whether it has been purchased or not
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Character1Btn()
    {
        for (int i = 0; i < AllCharacter.Length; i++)
        {
            AllCharacter[i].SetActive(false);
        }
        AllCharacter[0].SetActive(true);
        characterMovement.PlayerAnim = AllCharacter[0].GetComponent<Animator>();
        Debug.Log("Player Anim 1 " + characterMovement.PlayerAnim.name);


    }
    
    public void Character2Btn()
    {
        for (int i = 0; i < AllCharacter.Length; i++)
        {
            AllCharacter[i].SetActive(false);
        }
        AllCharacter[1].SetActive(true);
        characterMovement.PlayerAnim = AllCharacter[1].GetComponent<Animator>();
        Debug.Log("Player Anim 2  " + characterMovement.PlayerAnim.name);

    }

    public void Character3Btn()
    {
        for (int i = 0; i < AllCharacter.Length; i++)
        {
            AllCharacter[i].SetActive(false);
        }
        AllCharacter[2].SetActive(true);
        characterMovement.PlayerAnim = AllCharacter[2].GetComponent<Animator>();
        Debug.Log("Player Anim 3 " + characterMovement.PlayerAnim.name);

    }



    //--------------------------------------------------------//



    public void BtnBuySkin_1()
    {
        if (GlobleVariable.instance.Coins >= ShopItemsList[0].Price)
        {
            SetCoin(ShopItemsList[0].Price);
            ShopItemsList[0].LockImage.SetActive(false);
           // btnBuy_2.SetActive(false);
            PlayerPrefs.SetInt("isPurchasedSkin_1", 1);
            Debug.Log("its Buy");
            
        }
        else
        {
            Debug.Log("not enough money");
        }
    }

    public void BtnBuySkin_2()
    {
        if (GlobleVariable.instance.Coins >= ShopItemsList[1].Price)
        {
            SetCoin(ShopItemsList[1].Price);
            //  btnBuy_3.SetActive(false);
            ShopItemsList[1].LockImage.SetActive(false);
            PlayerPrefs.SetInt("isPurchasedSkin_2", 2);
        }
        else
        {
            Debug.Log("not enough money");
        }
    }
    public void BtnBuySkin_3()
    {
        if (GlobleVariable.instance.Coins >= ShopItemsList[2].Price)
        {
            SetCoin(ShopItemsList[2].Price);
            //  btnBuy_4.SetActive(false);
            ShopItemsList[2].LockImage.SetActive(false);
            PlayerPrefs.SetInt("isPurchasedSkin_3", 3);
        }
        else
        {
            Debug.Log("not enough money");
        }
    }
    void IsItPurchesed() //The function we check whether it has been purchased or not
    {
        isPurchasedSkin_0 = PlayerPrefs.GetInt("isPurchasedSkin_0", 0);
        if (isPurchasedSkin_0 != 0)
        {
           // btnBuy_2.SetActive(false);
            for (int i = 0; i < AllCharacter.Length; i++)
            {
                AllCharacter[i].SetActive(false);
            }
            AllCharacter[0].SetActive(true); // set chacter 1 by index
        }
      
        //---------------------------//
        isPurchasedSkin_1 = PlayerPrefs.GetInt("isPurchasedSkin_1", 0);
        if (isPurchasedSkin_1 != 0)
        {
           // btnBuy_3.SetActive(false);
            for (int i = 0; i < AllCharacter.Length; i++)
            {
                AllCharacter[i].SetActive(false);
            }
            AllCharacter[1].SetActive(true); // set chacter 1 by index
        }
      
        //---------------------------//
        isPurchasedSkin_2 = PlayerPrefs.GetInt("isPurchasedSkin_2", 0);
        if (isPurchasedSkin_2 != 0)
        {
           // btnBuy_4.SetActive(false);
            for (int i = 0; i < AllCharacter.Length; i++)
            {
                AllCharacter[i].SetActive(false);
            }
            AllCharacter[2].SetActive(true); // set chacter 1 by index
        }
    }
    void SetCoin(int price) // We are reducing coins after buying
    {
        GlobleVariable.instance.Coins -= price;
        UIManager.instance.CoinsTxt.text = "Coins: " + GlobleVariable.instance.Coins;
        PlayerPrefs.SetInt("Coin", GlobleVariable.instance.Coins);
    }





}
