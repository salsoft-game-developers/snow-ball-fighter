﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject m_Projectile;    // this is a reference to your projectile prefab
    public Transform m_SpawnTransform; // this is a reference to the transform where the prefab will spawn
    public float m_Speed;
    public bool IsBulletFire;
     
    /// <summary>
    /// Message that is called once per frame
    /// </summary>

    private void Start()
    {
     
    }
    private void Update()
    {
        if(IsBulletFire == true)
        {
            if (m_Speed <= 3)
            {
              //  m_Speed+=1f*Time.deltaTime;
              //  Debug.Log("Power : " + m_Speed);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            IsBulletFire = true;


            //if (m_Speed <= 3)
            //{
            //    m_Speed++;
            //    Debug.Log("Power : " +m_Speed);
            //}
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            GameObject m_ProjectileClone =  Instantiate(m_Projectile, m_SpawnTransform.position, m_SpawnTransform.rotation);
            m_ProjectileClone.GetComponent<Bullet>().m_Rigidbody.AddForce(m_ProjectileClone.GetComponent<Bullet>().m_Rigidbody.transform.forward * m_Speed * 1000);
            m_ProjectileClone.GetComponent<Bullet>().m_Rigidbody.AddForce(m_ProjectileClone.GetComponent<Bullet>().m_Rigidbody.transform.up * 2f, ForceMode.Impulse);
           
            IsBulletFire = false;
            m_Speed = .9f;
        }
    }

     public void BulletFire()
    {
        GameObject m_ProjectileClone = Instantiate(m_Projectile, m_SpawnTransform.position, m_SpawnTransform.rotation);
        m_ProjectileClone.GetComponent<Bullet>().m_Rigidbody.AddForce(m_ProjectileClone.GetComponent<Bullet>().m_Rigidbody.transform.forward * m_Speed * 950);
        m_ProjectileClone.GetComponent<Bullet>().m_Rigidbody.AddForce(m_ProjectileClone.GetComponent<Bullet>().m_Rigidbody.transform.up * 2.5f, ForceMode.Impulse);

        IsBulletFire = false;
        m_Speed = 1.4f;
    }
}
