﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobleVariable : MonoBehaviour
{
    public static GlobleVariable instance;


    public  int Coins = 0;
    public  int HighScore;
    public  int LevelCounter = 1;
    public  int EnemeyRange = 3;
    public  int Cash;
    public  int EnemiesReturnWithoutGift = 0;
    public  int EnemiesGetGift = 0;


    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Rest Values

        //PlayerPrefs.SetInt("highscore", 0);
        //PlayerPrefs.SetInt("EnimiesRange", 3);
        //PlayerPrefs.SetInt("LevelCounts", 1);
        //PlayerPrefs.SetInt("Coin", 0);
        //PlayerPrefs.SetInt("CashAmount", 0);

        //UIManager.instance.CoinsTxt.text = "Coins: " +Coins;
        //UIManager.instance.txtCashAmount.text = "Coins: " +Cash;

        HighScore = PlayerPrefs.GetInt("highscore", HighScore);
        EnemeyRange = PlayerPrefs.GetInt("EnimiesRange", EnemeyRange);
       // Coins = PlayerPrefs.GetInt("Coin", Coins);
        Cash = PlayerPrefs.GetInt("CashAmount", Cash);
        UIManager.instance.CoinsTxt.text = "Coins: " + Coins;
        UIManager.instance.txtCashAmount.text = "Coins: " + Cash;

        Debug.Log("highscore is : " + HighScore);
        Debug.Log("EnimiesRange is : " + EnemeyRange);
        Debug.Log("LevelCounts is : "+ LevelCounter);
        Debug.Log("CAash is : "+ Cash);
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
