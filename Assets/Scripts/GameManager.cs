﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    EnemyGeneration enemyGeneration;
    bool isCheckEnemies;
    public  bool isPlay;
    public bool IsPlayerDeath;
    public GameObject kidReturnPos;
    // public int Score = 0;
   [SerializeField] CharacterMovement character;
    public int EnemyReturnCounter;

    public float MinNumber = 0.1f;
    public float MaxNumber = 0.5f;

  //  public int kidzCounting = 0;

    // [SerializeField] GameObject m_Player;

    [Header ("FOR SPELL")]
    public float SpellTimeRemaning = 3, BigSpellTimeRemaning = 5;  // For Freez Spell Time
    public bool FreezingSpell,TimeIncreaseSpell, BigFreezSpell;
    public  GameObject FreezingSpellBtn,BigFreezingSpell,TimeMoreSpellBtn;
  
    public int freezSpellCount;
    public  int TimeSpellCount;
    public  int BigFreezTimeSpellCount;
    public GameObject nextbtnLevelCompleteScreen;

    public bool CoinsIntoCash;
    private void Awake()
    {
        Time.timeScale = 1;
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("cash is " + GlobleVariable.instance.Cash);
        UIManager.instance.txtCashAmount.text = "Cash: " + GlobleVariable.instance.Cash;
        enemyGeneration = GameObject.Find("EnemyGenerationObj").GetComponent<EnemyGeneration>();
        //StartCoroutine(WaitngForSpawnEnemies());
        IsPlayerDeath = false;
        GlobleVariable.instance.EnemiesGetGift=0;
        GlobleVariable.instance.EnemiesReturnWithoutGift = 0;
        BigFreezTimeSpellCount = PlayerPrefs.GetInt("FreezingTime8", 0);
        freezSpellCount = PlayerPrefs.GetInt("FreezingTime5", 0);
        TimeSpellCount = PlayerPrefs.GetInt("AddTime", 0);


        UIManager.instance.SmallFreezTxt.text = freezSpellCount.ToString();
        UIManager.instance.BigFreezTxt.text = freezSpellCount.ToString();
        UIManager.instance.AddTimeTxt.text = instance.freezSpellCount.ToString();

        if (freezSpellCount <= 0) {
            FreezingSpellBtn.GetComponent<Button>().interactable = false;       // freez Btn Not interactable
          
        } 
        if (BigFreezTimeSpellCount <= 0) {
            BigFreezingSpell.GetComponent<Button>().interactable = false;       // freez Btn Not interactable
          
        }


    }

    public void GameOverMethod()
    {
        if (enemyGeneration.enemeyCount == 0)
        {
            //  Debug.Log("before  Game Over");


            if (GlobleVariable.instance.EnemiesReturnWithoutGift > GlobleVariable.instance.EnemiesGetGift)
            {
                float randNum = Random.Range(MinNumber, MaxNumber);
                Debug.Log("Random num  is " + randNum);

                float tempCoin = 0;
                tempCoin = GlobleVariable.instance.Coins;
                Debug.Log("Temp Coins before " + tempCoin);

                tempCoin /= 8;
                Debug.Log("Temp Coins is " + tempCoin);
                GlobleVariable.instance.Cash = (int)tempCoin;
                UIManager.instance.GameOverScreen.SetActive(true);

                UIManager.instance.G_HappyKid.text = "" + GlobleVariable.instance.EnemiesGetGift;
                UIManager.instance.G_SadKid.text = "" + GlobleVariable.instance.EnemiesReturnWithoutGift;


                UIManager.instance.CoinsTxt.text = "Coins: " + GlobleVariable.instance.Coins;
                UIManager.instance.txtCashAmount.text = "Cash: $" + GlobleVariable.instance.Cash;
                PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);

                Debug.Log("Game Over");

                SoundManager.instance.BgSfx.Stop();
                isPlay = false;

            }
            else if (GlobleVariable.instance.EnemiesReturnWithoutGift < GlobleVariable.instance.EnemiesGetGift)
            {
                UIManager.instance.levelCompletePanel.SetActive(true);
                StartCoroutine(delayfornxtBtn());
                Debug.Log("its Win");

                float randNum = (int)Random.Range(MinNumber, MaxNumber);
                Debug.Log("Random num  is " + randNum);
                float tempCoin = 0;
                tempCoin = GlobleVariable.instance.Coins;
                Debug.Log("Temp Coins before " + tempCoin);
                tempCoin /= 8;
                Debug.Log("Temp Coins is " + tempCoin);
                GlobleVariable.instance.Cash += (int)tempCoin;
                UIManager.instance.levelCompletedCashTxt.text = "Cash: " + GlobleVariable.instance.Cash;
                PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);

                UIManager.instance.levelCompletedCoinTxt.text = "Coins: " + GlobleVariable.instance.Coins;
                UIManager.instance.CoinsTxt.text = "Coins: " + GlobleVariable.instance.Coins;

                Debug.Log("cash is " + GlobleVariable.instance.Cash);

                StartCoroutine(waitToCountersZero());

            }
        }

    }
    // Update is called once per frame
    void Update()
    {


        if (GlobleVariable.instance.Coins > GlobleVariable.instance.HighScore)
        {
            GlobleVariable.instance.HighScore = GlobleVariable.instance.Coins;
            UIManager.instance.CoinsTxt.text = "Coins: " + GlobleVariable.instance.Coins;
          
        }


        //if(enemyGeneration.enemeyCount == 0)
        //{
        //    //  Debug.Log("before  Game Over");
         

        //    if (GlobleVariable.instance.EnemiesReturnWithoutGift > GlobleVariable.instance.EnemiesGetGift)
        //    {
        //        float randNum = Random.Range(MinNumber, MaxNumber);
        //        float tempCoin = 0;
        //        tempCoin = GlobleVariable.instance.Coins;
        //        tempCoin *= randNum;
        //        GlobleVariable.instance.Cash = (int)tempCoin;
        //        UIManager.instance.GameOverScreen.SetActive(true);

        //        UIManager.instance.G_HappyKid.text = "" + GlobleVariable.instance.EnemiesGetGift;
        //        UIManager.instance.G_SadKid.text = "" + GlobleVariable.instance.EnemiesReturnWithoutGift;
                
                
        //        UIManager.instance.CoinsTxt.text = "Coins: " + GlobleVariable.instance.Coins;
        //        UIManager.instance.txtCashAmount.text = "Cash: $" + GlobleVariable.instance.Cash;
        //        PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);
                
        //        Debug.Log("Game Over");
               
        //        SoundManager.instance.BgSfx.Stop();
        //        isPlay = false;

        //    }
        //    else if(GlobleVariable.instance.EnemiesReturnWithoutGift < GlobleVariable.instance.EnemiesGetGift)
        //    {
        //        UIManager.instance.levelCompletePanel.SetActive(true);
        //        StartCoroutine(delayfornxtBtn());
        //        Debug.Log("its Win");
               
        //        float randNum = (int)Random.Range(MinNumber, MaxNumber);
        //        float tempCoin = 0;
        //        tempCoin = GlobleVariable.instance.Coins;
        //        tempCoin *= randNum;
        //        GlobleVariable.instance.Cash += (int)tempCoin;
        //        UIManager.instance.levelCompletedCashTxt.text = "Cash: " + GlobleVariable.instance.Cash;
        //        PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);

        //        UIManager.instance.levelCompletedCoinTxt.text ="Coins: "+GlobleVariable.instance.Coins;
        //        UIManager.instance.CoinsTxt.text ="Coins: "+GlobleVariable.instance.Coins;
               
        //        Debug.Log("cash is " + GlobleVariable.instance.Cash);

        //        StartCoroutine(waitToCountersZero());

        //    }
        

        if (FreezingSpell == true)  //// 5 sec Freez
        {
            FreezingSpellBtn.GetComponent<Button>().interactable = false;       // freez Btn Not interactable
            BigFreezingSpell.GetComponent<Button>().interactable = false;       // freez Btn Not interactable
            SpellTimeRemaning -= Time.deltaTime;    // For freez spell
            UIManager.instance.SpellFreezTimeTxt.text = SpellTimeRemaning.ToString();
            Debug.Log("Spell Time " + SpellTimeRemaning);
        }
        if(SpellTimeRemaning <= 0)
        {
            FreezingSpell = false;
            SpellTimeRemaning = 5;
            UIManager.instance.SpellFreezTimeTxt.text = SpellTimeRemaning.ToString();
            FreezingSpellBtn.GetComponent<Button>().interactable = true;   // freez Btn  interactable
            BigFreezingSpell.GetComponent<Button>().interactable = true;   // freez Btn  interactable
        }
     

        
        if (BigFreezSpell == true)      // 8 sec Freez
        {
            BigFreezingSpell.GetComponent<Button>().interactable = false;       // freez Btn Not interactable
            FreezingSpellBtn.GetComponent<Button>().interactable = false;       // freez Btn Not interactable
            BigSpellTimeRemaning -= Time.deltaTime;    // For freez spell
            Debug.Log("Spell Time " + BigSpellTimeRemaning);
            UIManager.instance.BigSpellFreezTimeTxt.text = BigSpellTimeRemaning.ToString();
        }
        if(BigSpellTimeRemaning <= 0)
        {
            FreezingSpell = false;
            BigSpellTimeRemaning = 8;
            UIManager.instance.BigSpellFreezTimeTxt.text = BigSpellTimeRemaning.ToString();

            BigFreezingSpell.GetComponent<Button>().interactable = true;   // freez Btn  interactable
            FreezingSpellBtn.GetComponent<Button>().interactable = true;   // freez Btn  interactable
        }


        //if (UIManager.instance.GiftPrefab.GetComponent<GiftManager>().giftHealth <= 0 && GameManager.instance.isPlay == true)
        //{
        //    UIManager.instance.GameOverScreen.SetActive(true);
        //    Debug.Log("Gift Health " + UIManager.instance.GiftPrefab.GetComponent<GiftManager>().giftHealth);
        //   // Destroy(enemyGeneration.enimiesParentObj, .2f);
        //}

        //if (UIManager.instance.GameOverScreen.activeInHierarchy == true)
        //{
        //    for (int i = 0; i < enemyGeneration.holdEnimiesRef.Count; i++)
        //    {
        //        enemyGeneration.holdEnimiesRef.RemoveAt(i);
        //      //  Destroy(enemyGeneration.holdEnimiesRef[i]);
        //    }
        //    enemyGeneration.holdEnimiesRef = new List<GameObject>();


        //   // GameManager.instance.IsPlayerDeath = false;
        //    //  enemyGeneration.GenerateEnemy();
        //  //  enemyGeneration.ResetGenerateEnemy();
        //}

        //if(IsPlayerDeath == true)
        //{
        //    UIManager.instance.GameOverScreen.SetActive(true);
        //}


        //  UIManager.instance.ScoreTxt.text = "Score :" + GlobleVariable.instance.Score;
        //if (enemyGeneration.enemeyCount == 0 && isCheckEnemies == true)
        //{
        //    // Level Complete
        //    Debug.Log("Level Complete");
        //    UIManager.instance.levelCompletePanel.SetActive(true);

        //}
    }

    IEnumerator delayfornxtBtn()
    {
        yield return new WaitForSeconds(2f);
        nextbtnLevelCompleteScreen.SetActive(true);
    }

    IEnumerator WaitngForSpawnEnemies()
    {
        yield return new WaitForSeconds(1);
        isCheckEnemies = true;

    }
    
    public void TimeIncreaseSpellBtn()
    {
        if(TimeSpellCount > 0)
        {
            TimeIncreaseSpell = true;

        }
    }


    public void FreezTimeBtn()
    {
        if (freezSpellCount > 0)
        {
            FreezingSpell = true;
        }

    }
    public void BigFreezBtn()
    {
        if (BigFreezTimeSpellCount > 0)
        {
            BigFreezSpell = true;
        }
    }
     
    IEnumerator waitToCountersZero()
    {
        yield return new  WaitForSeconds(.5f);

        GlobleVariable.instance.EnemiesReturnWithoutGift = 0;
        GlobleVariable.instance.EnemiesGetGift = 0;
    }


    public void GetCashFormCoin()
    {
        float randNum = (int)Random.Range(MinNumber, MaxNumber);
        float tempCoin = 0;
        tempCoin = GlobleVariable.instance.Coins;
        tempCoin *= randNum;
        GlobleVariable.instance.Cash += (int)tempCoin;
        UIManager.instance.levelCompletedCashTxt.text = "Cash: " + GlobleVariable.instance.Cash;
        PlayerPrefs.SetInt("CashAmount", GlobleVariable.instance.Cash);
    }
}
