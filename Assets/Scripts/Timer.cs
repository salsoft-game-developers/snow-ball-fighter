﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    //public static Timer instance;

    public float timeRemaining = 20;
    public bool timerIsRunning = false;
    public Text timeText;
    EnemyGeneration enemyGeneration;
    public Slider timeSlider;
  
 

    private void Awake()
    {
        //if (instance != null)
        //{
        //    Destroy(gameObject);
        //}
        //else
        //{
        //    instance = this;
        //}
    }
    private void Start()
    {
        // Starts the timer automatically
        timerIsRunning = true;
        enemyGeneration = GameObject.Find("EnemyGenerationObj").GetComponent<EnemyGeneration>();
        timeSlider.maxValue = timeRemaining;
       // timeSlider.value = timeSlider.maxValue;
        Debug.Log("Value Is "+ timeSlider.value);
    }

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                Debug.Log("before Enemey Counting " + enemyGeneration.enemeyCount);
                Debug.Log("Slider Value " + timeSlider.maxValue);
                if (enemyGeneration.enemeyCount != 0 || UIManager.instance.GameOverScreen.activeInHierarchy == false || GameManager.instance.FreezingSpell == true)
                {
                   //  StartCoroutine(WaitforFreezTimeofEnemy());
                    if (GameManager.instance.FreezingSpell == false)
                    {
                       timeRemaining -= Time.deltaTime;
                        timeSlider.value -= Time.deltaTime;
                        // Debug.Log("Enemey Counting " + enemyGeneration.enemeyCount);
                    }
                    if (GameManager.instance.TimeIncreaseSpell == true)
                    {
                        GameManager.instance.TimeIncreaseSpell = false;
                        timeRemaining += 5;
                    }
                }
                DisplayTime(timeRemaining);
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
               
            }
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        //timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        timeText.text = "" + seconds;
    }

    IEnumerator WaitforFreezTimeofEnemy()
    {
        yield return new WaitForSeconds(5f);
        GameManager.instance.FreezingSpell = false;
        Debug.Log("its Freezing");
    }
}