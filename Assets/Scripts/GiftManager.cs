﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftManager : MonoBehaviour
{

    public int giftHealth = 100;
    EnemyGeneration enemyGeneration;


    // Start is called before the first frame update
    void Start()
    {
        giftHealth = 100;
        enemyGeneration = GameObject.Find("EnemyGenerationObj").GetComponent<EnemyGeneration>();

    }

    // Update is called once per frame
    void Update()
    {
        //if(giftHealth <= 0)
        //{
        //    GameManager.instance.isPlay = false;
        //}

        //if(giftHealth <=0 && GameManager.instance.isPlay==true)
        // {
        //     UIManager.instance.GameOverScreen.SetActive(true);
        //     for (int i = 0; i < enemyGeneration.holdEnimiesRef.Count; i++)
        //     {
        //         Destroy(enemyGeneration.holdEnimiesRef[i].gameObject);
        //         enemyGeneration.holdEnimiesRef.RemoveAt(i);

        //         Debug.Log("Enimies is destory");
        //     }

        //  Destroy(enemyGeneration.enimiesParentObj, .1f);
        // GameManager.instance.isPlay = false;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet")
        {
            //take damage
            TakeDamage(5);
            Destroy(other.gameObject);
        }
    }


    public void TakeDamage(int giftdamage)
    {
        giftHealth -= giftdamage;

        if (giftHealth <= 0)
        {
            //  gameObject.SetActive(false);
            // Destroy(gameObject, .1f);
            StartCoroutine(waitForUnactive());
        }
    }
    IEnumerator waitForUnactive()
    {
        yield return new WaitForSeconds(.5f);
        gameObject.SetActive(false);

    }

}

