﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    [SerializeField] GameObject MainCharcter;
    [SerializeField] Shooting shooting;
    [Header ("UI Screen")]
    public GameObject mainMenuScreen,levelCompletePanel,GameOverScreen,NextLevelScreen,GamePlayScreen,ShopScreen, PauseScreen,IAP_Screen;
    [SerializeField] EnemyGeneration enemyGeneration;
    public bool IsFiring;
    public GameObject GiftPrefab,GiftSpawnPoint;
    public GameObject GiftprefabClone;
    public  Text CoinsTxt,NextLevelTxt,HighScoreText,LevelNameTxt;
    public CharacterMovement characterMovement;
    public Text txtCashAmount;
    public Text HappyKid;
    public Text SadKid;
    public Text ShopCashTxt;

    [Header("Level Complete Screen")]
   public  Text levelCompletedCoinTxt;
   public  Text levelCompletedCashTxt;


    [Header ("Game over Text")]

    public Text G_HappyKid;
    public Text G_SadKid;
    public int SpellCounter;

    [Header("Spell Section")]
    public Text SmallFreezTxt;
    public Text BigFreezTxt;
    public Text AddTimeTxt;
 
    public Text SpellFreezTimeTxt;
    public Text BigSpellFreezTimeTxt;
    //   public Text MoreTimeTxt;
    public int adsCountNumber;
    public GameObject RewardBtnOnLevelComplete;
    public GameObject TutScreen;
    public int TutCount = 0;
    public GameObject CashAnimation;

    public int IntersAdsCount;


    bool isTabletOrIpad;

    //public GameObject locationPanel;
    //public RectTransform[] logos;

    public GameObject IntresAddObj;

    private void Awake()
    {
        CashAnimation.SetActive(false);
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }


    private void Start()
    {
        IntersAdsCount = PlayerPrefs.GetInt("AdsCountInters", 0);

#if UNITY_IOS
        //   isTabletOrIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad"); //Ipad

#elif UNITY_ANDROID
        float aspectRatio = Mathf.Max(Screen.width, Screen.height) / Mathf.Min(Screen.width, Screen.height);
        isTabletOrIpad = (DeviceDiagonalSizeInInches() > 6.5f && aspectRatio < 2f);
#endif
        // if (isTabletOrIpad) //Tab
        // {

        //    Vector3 pos1 = locationPanel.GetComponent<RectTransform>().localPosition;
        //    pos1 = new Vector3(0, -135, 0);
        //   locationPanel.GetComponent<RectTransform>().localPosition = pos1;
        //   locationPanel.GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0);

        //   for (int i = 0; i < logos.Length; i++)
        //    {

        //       logos[i].localPosition = new Vector3(0, logos[i].localPosition.y + 190f, 0);

        //  }


        // }


        // PlayerPrefs.SetInt("LevelCounts", 0);
        levelCompletePanel.SetActive(false);
        GameOverScreen.SetActive(false);
        GamePlayScreen.SetActive(false);
        mainMenuScreen.SetActive(true);
        ShopScreen.SetActive(false);
        GiftprefabClone = Instantiate(GiftPrefab, GiftSpawnPoint.transform.position, GiftSpawnPoint.transform.rotation);
        GlobleVariable.instance.LevelCounter =  PlayerPrefs.GetInt("LevelCounts", GlobleVariable.instance.LevelCounter);
        HighScoreText.text = "HighScore : " + GlobleVariable.instance.HighScore;
        LevelNameTxt.text = "Level: " + GlobleVariable.instance.LevelCounter;
        txtCashAmount.text = "Cash: " + GlobleVariable.instance.Cash;
        CoinsTxt.text = "Coins: " + GlobleVariable.instance.Coins;
        HappyKid.text = "" + GlobleVariable.instance.EnemiesGetGift;
        SadKid.text = "" + GlobleVariable.instance.EnemiesReturnWithoutGift;
        TutCount = PlayerPrefs.GetInt("TutorialPanel");
        ShopCashTxt.text = "Cash: $ " + GlobleVariable.instance.Cash;

    }


    private void Update()
    {
       
    }



    public void PlayBtn()
    {
        SoundManager.instance.ClickSfx.Play();
        GameManager.instance.isPlay = true;
        mainMenuScreen.SetActive(false);
        GameManager.instance.CoinsIntoCash = true;

        //  StartCoroutine(enemyGeneration.WaitingTime());
        if (TutCount > 0)
        {
            TutScreen.SetActive(false);
          //  PlayerPrefs.SetInt("TutorialPanel", TutCount);
        }

        StartCoroutine(enemyGeneration.ResetGenerationWaitingTime());

        //int RandNum = Random.Range(0, enemyGeneration.spawnPoints.Length);

        //enemyGeneration.enemyClone = Instantiate(enemyGeneration.EnemyPrefab, enemyGeneration.spawnPoints[RandNum].transform.position, enemyGeneration.spawnPoints[RandNum].transform.rotation);
        //enemyGeneration.enemyClone.transform.parent = enemyGeneration.enimiesParentObj.transform;
        //enemyGeneration.holdEnimiesRef.Add(enemyGeneration.enemyClone);

        //enemyGeneration.enemeyCount++;
       // GameManager.instance.isPlay = true;
        GamePlayScreen.SetActive(true);
      //  GameManager.instance.EnemyHealthCounter = 0;

    }

    public void MenuBtn()
    {
        Time.timeScale = 1;
        SoundManager.instance.ClickSfx.Play();
        IntersAdsCount++;
        PlayerPrefs.SetInt("AdsCountInters", IntersAdsCount);
        if (IntersAdsCount > 3)
        {
            IntresAddObj.GetComponent<InterstitialAd>().ShowAd();
            IntersAdsCount = 0;
            PlayerPrefs.SetInt("AdsCountInters", IntersAdsCount);


        }
        else
        {
            SceneManager.LoadScene(0);
        }
        
        //GameOverScreen.SetActive(false);
        //PauseScreen.SetActive(false);
        //Debug.Log("its Menu BTn");
      
        //characterMovement.PlayerAnim.SetTrigger("Idle");

        //MainCharcter.transform.position = new Vector3(0f, 1f, 2.3f);
        //enemyGeneration.enemeyCount = 0;
        //for (int i = 0; i < enemyGeneration.holdEnimiesRef.Count; i++)
        //{
        //    enemyGeneration.holdEnimiesRef.RemoveAt(i);
        //}
        //enemyGeneration.holdEnimiesRef = new List<GameObject>();

    }

    public void ShopBtn()
    {
        // Shop panel active
        SoundManager.instance.ClickSfx.Play();
        mainMenuScreen.SetActive(false);
        ShopScreen.SetActive(true);
        ShopCashTxt.text = "Cash: " + GlobleVariable.instance.Cash;

    }

    public void ShopCloseBtn()
    {
        ShopScreen.SetActive(false);
        mainMenuScreen.SetActive(true);
    }

    public void pauseBtn()
    {
        SoundManager.instance.ClickSfx.Play();
        PauseScreen.SetActive(true);
        Time.timeScale = 0;
    }
    public void ClosePausedScreen()
    {
        SoundManager.instance.ClickSfx.Play();

        PauseScreen.SetActive(false);
        Time.timeScale = 1;
    }

    public void ExitBtn()
    {
        SoundManager.instance.ClickSfx.Play();

        Application.Quit();
    }

    public void RestartGameBtn()
    {
        SoundManager.instance.ClickSfx.Play();

        GameOverScreen.SetActive(false);
        SceneManager.LoadScene(0);
    }

    public void GetCashBtn()
    {
        ShopScreen.SetActive(false);
        IAP_Screen.SetActive(true);
    }

    public void GoToNextLevel()
    {
        SoundManager.instance.ClickSfx.Play();

        levelCompletePanel.SetActive(false);


        GameManager.instance.CoinsIntoCash = true;



        Destroy(GiftprefabClone);
        NextLevelScreen.SetActive(true);
        PlayerPrefs.SetInt("highscore",GlobleVariable.instance.HighScore);
        GlobleVariable.instance.LevelCounter++;
        PlayerPrefs.SetInt("LevelCounts", GlobleVariable.instance.LevelCounter);
        NextLevelTxt.text = "Level : " + GlobleVariable.instance.LevelCounter;
        LevelNameTxt.text = "Level : " + GlobleVariable.instance.LevelCounter;
        txtCashAmount.text = "Cash: " + GlobleVariable.instance.Cash;
        GlobleVariable.instance.Coins = 0;
        GameManager.instance.isPlay = true;
      //  GameManager.instance.EnemyHealthCounter = 0;

    }

    public void ResetLevelBtn() 
    {
       
        SoundManager.instance.ClickSfx.Play();
        GlobleVariable.instance.Coins = 0;
        UIManager.instance.CoinsTxt.text = "Coins: " + GlobleVariable.instance.Coins;
        GameOverScreen.SetActive(false);
        SoundManager.instance.BgSfx.Play();
        characterMovement.PlayerAnim.SetTrigger("Idle");

        // GiftprefabClone = Instantiate(GiftPrefab, GiftSpawnPoint.transform.position, GiftSpawnPoint.transform.rotation);
        if (GiftprefabClone.activeInHierarchy == true)
        {
            GiftprefabClone.GetComponent<GiftManager>().giftHealth = 100;
            Debug.Log("if");
        }
        else if(GiftprefabClone.activeInHierarchy == false)
        {
            Debug.Log("else");
            GiftprefabClone.SetActive(true);
            GiftprefabClone.GetComponent<GiftManager>().giftHealth = 100;
           // GiftprefabClone = Instantiate(GiftPrefab, GiftSpawnPoint.transform.position, GiftSpawnPoint.transform.rotation);
        }

        MainCharcter.transform.position = new Vector3(0f, 1f,2.3f);
        enemyGeneration.enemeyCount = 0;
        GlobleVariable.instance.EnemiesGetGift = 0;
        GlobleVariable.instance.EnemiesReturnWithoutGift = 0;

        // enemyGeneration.GenerateEnemy();
        for (int i = 0; i <enemyGeneration.holdEnimiesRef.Count; i++)
        {
            enemyGeneration.holdEnimiesRef.RemoveAt(i);
        }
        enemyGeneration.holdEnimiesRef = new List<GameObject>();

       
        GameManager.instance.IsPlayerDeath = false;
        GameManager.instance.isPlay = true;
        //  enemyGeneration.GenerateEnemy();
        enemyGeneration.ResetGenerateEnemy();
        GlobleVariable.instance.EnemiesGetGift = 0;
        GlobleVariable.instance.EnemiesReturnWithoutGift = 0;
        // GameManager.instance.EnemyHealthCounter = 0;


        //ShopManager.Instance.characterMovement.PlayerHealth = 100;
        //for (int i = 0; i < ShopManager.Instance.AllCharacter.Length; i++)
        //{
        //    if (ShopManager.Instance.AllCharacter[i].activeInHierarchy)
        //    {
        //        ShopManager.Instance.characterMovement.PlayerAnim.SetTrigger("Idle");
        //        Debug.Log("is idle of player");
        //    }
        //}

        //PlayerAnim.SetTrigger("IsDeath");


    }

    public void GoBtn()
    {
        SoundManager.instance.ClickSfx.Play();
        MainCharcter.transform.position = new Vector3(-2.77f, 1f, 2.3f);
        characterMovement.PlayerAnim.SetTrigger("Idle");
        GiftprefabClone = Instantiate(GiftPrefab, GiftSpawnPoint.transform.position, GiftSpawnPoint.transform.rotation);
        NextLevelScreen.SetActive(false);
        SoundManager.instance.BgSfx.Play();

        //  enemyGeneration.EnemeyRange += 2;
        if (GlobleVariable.instance.EnemeyRange <= 9)
        {
            GlobleVariable.instance.EnemeyRange += 1;
          
            PlayerPrefs.SetInt("EnimiesRange",GlobleVariable.instance.EnemeyRange);
        }

        //if (GameManager.instance.EnemyReturnCounter < 10)
        //{
        //    GameManager.instance.EnemyReturnCounter += 1;
        //}
        //enemyGeneration.EnemiesHealthMultiplier += 100;
        enemyGeneration.GenerateEnemy();
       // enemyGeneration.enemyClone.GetComponent<EnemyAiTutorial>().Gift = GiftprefabClone.transform;
        // enemyGeneration.enemyClone.GetComponent<EnemyAiTutorial>().Enemyhealth += 100;
      //  Debug.Log("Enemy new health " + enemyGeneration.enemyClone.GetComponent<EnemyAiTutorial>().Enemyhealth);
        GlobleVariable.instance.EnemiesGetGift = 0;
        GlobleVariable.instance.EnemiesReturnWithoutGift = 0;
    }

    public void IAPClosedBtn()
    {
        IAP_Screen.SetActive(false);
        ShopScreen.SetActive(true);

    }

}
