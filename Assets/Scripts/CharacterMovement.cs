﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    Vector3 dist;
    Vector3 startPos;
    float posX;
    float posZ;
    float posY;
    public  float PlayerHealth;
    [SerializeField] int TakeDamagePlayer;
    Shooting shooting;
    public  Animator PlayerAnim;

    private void Start()
    {
        PlayerAnim = gameObject.transform.GetChild(0).GetComponent<Animator>();
        shooting =  GetComponent<Shooting>();
    }

    //Player Movement

    void OnMouseDown()
    {
        startPos = transform.position;
        dist = Camera.main.WorldToScreenPoint(transform.position);
        posX = Input.mousePosition.x - dist.x;
        posY = Input.mousePosition.y - dist.y;
        posZ = Input.mousePosition.z - dist.z;
    }

    void OnMouseDrag()
    {
        if (GameManager.instance.isPlay == true )
        {
            shooting.IsBulletFire = true;
            float disX = Input.mousePosition.x - posX;
            float disY = Input.mousePosition.y - posY;
            float disZ = Input.mousePosition.z - posZ;

            Vector3 lastPos = Camera.main.ScreenToWorldPoint(new Vector3(disX, disY, disZ));
            transform.position = new Vector3(lastPos.x, startPos.y, lastPos.z);
        }
    }
    private void OnMouseUp()
    {
        if (GameManager.instance.isPlay == true)
        {
            PlayerAnim.SetTrigger("ballShoot");
            UIManager.instance.TutCount = 1;
            PlayerPrefs.SetInt("TutorialPanel",UIManager.instance.TutCount);
            UIManager.instance.TutScreen.SetActive(false);

            //  shooting.BulletFire();
            StartCoroutine(waitingForShoot());

            // shooting.BulletFire();
        }
    }
    public void PlayerShootBall()
    {
       // SoundManager.instance.ThrowSfx.Play();
        shooting.BulletFire();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet" )
        {
            Debug.Log("Hit with enemy Bullet");
            // hit with enemy bullet
            Destroy(other.gameObject);
          //  TakeDamage(TakeDamagePlayer);

        }
    }


    //public void TakeDamage(int Damage)
    //{
    //    PlayerHealth -= Damage;

    //    if (PlayerHealth <= 0)
    //    {
    //        //  Destroy(gameObject, 0.2f);
    //        SoundManager.instance.DeathSfx.Play();
    //        PlayerAnim.SetTrigger("IsDeath");
    //        GameManager.instance.IsPlayerDeath = true;
    //        UIManager.instance.GameOverScreen.SetActive(true);
    //        GameManager.instance.isPlay = false;
    //        Debug.Log("Player is death " + GameManager.instance.IsPlayerDeath);
    //        // Debug.Log("player Hit ?Me");
    //    }
    //}
    IEnumerator waitingForShoot()
    {
        yield return new WaitForSeconds(.4f);
        SoundManager.instance.ThrowSfx.Play();
        //  SoundManager.instance.ThrowSfx.Play();
        shooting.BulletFire();
    }
}
