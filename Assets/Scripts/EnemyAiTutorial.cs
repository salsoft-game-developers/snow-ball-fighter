﻿
using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class EnemyAiTutorial : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    public Transform Gift,ReturnPos;

    public LayerMask whatIsGround, whatIsPlayer , WhatIsGift,WhatIsReturnPos;

    public float Enemyhealth;

    public float WaitingTimeOfGift;

    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    //Attacking
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject projectile;
    public bool Iscollecting; // check enemy return count

    //States
    public float sightRange, attackRange, g_sightRange, m_attackRange, k_ReturnInSightRange;
    public bool /*playerInSightRange, playerInAttackRange,*/ GiftInSightRange, GiftInAttackRange, ReturnInSightRange, ReturnObjInAttackRange;

    EnemyGeneration enemyGeneration;
    [SerializeField] Animator EnemyAnim;
    [SerializeField] GameObject EnemyThrowPoint;
    [SerializeField] GameObject hitParticleEffect;
    [SerializeField] Transform HitParticleSpawnPoint;
    [SerializeField] bool isReturn;
    [SerializeField] NavMeshAgent meshAgent;
    [SerializeField] Timer timer;
    [SerializeField] bool GetGifted;
    [SerializeField] bool iHaveGift;// check is return with gift or not
   
    
    
    private void Awake()
    {
        player = GameObject.Find("Character").transform;
    
        agent = GetComponent<NavMeshAgent>();
       
        enemyGeneration = GameObject.Find("EnemyGenerationObj").GetComponent<EnemyGeneration>();
      
    }
    private void Start()
    {
        Gift = UIManager.instance.GiftprefabClone.transform;
        ReturnPos = GameManager.instance.kidReturnPos.transform;
        meshAgent.stoppingDistance = 25;
    }

    private void Update()
    {
        //Check for sight and attack range
       
        //    playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        //    playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);
        //    if (!playerInSightRange && !playerInAttackRange) Patroling();

        //    if (playerInSightRange && !playerInAttackRange) ChasePlayer();
        //    if (playerInAttackRange && playerInSightRange) AttackPlayer();


        if (GameManager.instance.IsPlayerDeath == false && isReturn == false)
        {
            GiftInSightRange = Physics.CheckSphere(transform.position, g_sightRange, WhatIsGift);
            GiftInAttackRange = Physics.CheckSphere(transform.position, m_attackRange, WhatIsGift);

            if (!GiftInSightRange && !GiftInAttackRange) Patroling();

            if (GiftInSightRange && !GiftInAttackRange) ChaseGift();
            if (GiftInAttackRange && GiftInSightRange) AttackGift();
            //{
            //    // AttackGift();
            //    Debug.Log("Kid Stop");
            //    EnemyAnim.SetBool("IsRunning", false);
                
            //}

        }
        if (isReturn == true && timer.timerIsRunning == true)
        {

            if (!ReturnInSightRange) Patroling();

            if (ReturnInSightRange) ChaseReturnPos();
            if (ReturnObjInAttackRange && ReturnInSightRange)
            {

                EnemyAnim.SetBool("IsRunning", false);
                // Destroy(gameObject, 1);
            }
        }

        if (timer.timerIsRunning == false || timer.timeRemaining<=0 /*&& Iscollecting == false*/)
        {
           // Iscollecting = true;
         //   GlobleVariable.instance.EnemiesHealthCount++;
           
            Debug.Log("timer is : " + timer.timerIsRunning);

            EnemyAnim.SetBool("IsRunning", true);
           // meshAgent.stoppingDistance = 5;
            isReturn = true;
            ReturnInSightRange = true;
           
            Destroy(gameObject, 1);


            //if (GlobleVariable.instance.EnemiesReturnWithoutGift >= GameManager.instance.EnemyReturnCounter)
            //{
            //    UIManager.instance.GameOverScreen.SetActive(true);
            //    SoundManager.instance.BgSfx.Stop();
            //}


            //if (GlobleVariable.instance.EnemiesReturnWithoutGift < GlobleVariable.instance.EnemiesGetGift) // check to which one is greater 
            // {
            //     UIManager.instance.levelCompletePanel.SetActive(true);
            // }

            if (!ReturnInSightRange) Patroling();

            if (ReturnInSightRange) ChaseReturnPos();
            //    if (ReturnObjInAttackRange && ReturnInSightRange)
            //    {
            //        // AttackGift();
            //        //Debug.Log("Kid Stop");
            //        EnemyAnim.SetBool("IsRunning", false);
            //        meshAgent.stoppingDistance = 5;
            //      //  isReturn = true;
            //        ReturnInSightRange = true;
            //        //   StartCoroutine(waitForReturn());
            //        // Destroy(gameObject, 1);
            //    }
            //Timer.instance.SadPlayer = false;
            //}

            //    //}
            //    //  if (!playerInSightRange && !playerInAttackRange) Patroling();
            //    // if (playerInSightRange && !playerInAttackRange) ChaseGift();
            //    // if (playerInAttackRange && playerInSightRange) AttackGift();
           
        }
    }

        private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
       agent.SetDestination(player.position);
      //  agent.SetDestination(Gift.position);
    }
    private void ChaseGift()
    {
     //  agent.SetDestination(player.position);
        agent.SetDestination(Gift.position);
        EnemyAnim.SetBool("IsRunning", true);

    } private void ChaseReturnPos()
    {
     //  agent.SetDestination(player.position);
        agent.SetDestination(ReturnPos.position);
        EnemyAnim.SetBool("IsRunning", true);

    }

    private void AttackPlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);
        

        transform.LookAt(player);
       // transform.LookAt(Gift); // Attacking Gift to destroy

        if (!alreadyAttacked)
        {
          
            

            ///Attack code here
           // Rigidbody rb = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            Rigidbody rb = Instantiate(projectile, EnemyThrowPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            rb.AddForce(transform.up * 2f, ForceMode.Impulse);
            ///End of attack code

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }  
    private void AttackGift()
    {
        //Make sure enemy doesn't move
        if (GameManager.instance.IsPlayerDeath == false)
        {
            agent.SetDestination(transform.position);


            //transform.LookAt(player);
            transform.LookAt(Gift); // Attacking Gift to destroy
            EnemyAnim.SetBool("IsRunning", false);

            //if (!alreadyAttacked)
            //{
            //    alreadyAttacked = true;
            //    Debug.Log("Check1");
            //    if (player.GetComponent<CharacterMovement>().PlayerHealth > 0 && GameManager.instance.IsPlayerDeath==false)
            //    {
            //        EnemyAnim.SetTrigger("ThrowBall");
            //    }
            //    StartCoroutine(waitingForEnemiesThrow());
            //    ///Attack code here
            //    //Rigidbody rb = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            //    //rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            //    //rb.AddForce(transform.up * 2f, ForceMode.Impulse);
            //    /////End of attack code


            //    //Invoke(nameof(ResetAttack), timeBetweenAttacks);
            //}
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(int damage)
    {
        Enemyhealth -= damage;

        if (Enemyhealth <= 0)
        {
           // Invoke(nameof(DestroyEnemy), 0.1f);
           // Debug.Log("player Hit ?Me");
        }
    }
    //private void DestroyEnemy()
    //{ 
    //    EnemyAnim.SetTrigger("EnemyDeath");
    //   // Destroy(gameObject,1);

    //    enemyGeneration.enemeyCount--;
    //    //GameManager.instance.Score++;
    //    GlobleVariable.instance.Score++;
    //    UIManager.instance.ScoreTxt.text = "Score :" + GlobleVariable.instance.Score;
    //    if (enemyGeneration.enemeyCount == 0)
    //    {

    //        player.GetComponent<CharacterMovement>().PlayerAnim.SetTrigger("IsWinning");
            
    //        UIManager.instance.levelCompletePanel.SetActive(true);
    //        SoundManager.instance.WinningSfx.Play();
    //        GlobleVariable.instance.LevelCounter++;
    //        PlayerPrefs.SetInt("LevelCounts", GlobleVariable.instance.LevelCounter);
    //        Debug.Log("Levles :" + GlobleVariable.instance.LevelCounter);
    //        GameManager.instance.isPlay = false;
    //    }
    //    Debug.Log("Enemy Number : " + enemyGeneration.enemeyCount);
    //}

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "PlayerBullet")
        {
            // player hit enemy
            iHaveGift = true;

            GlobleVariable.instance.EnemiesGetGift++; // enemy get gift counter to check how many enemies get gift
            Debug.Log("player Hit Me");
            UIManager.instance.HappyKid.text = "" + GlobleVariable.instance.EnemiesGetGift;
            Instantiate(hitParticleEffect, HitParticleSpawnPoint.transform.position, Quaternion.identity);
            timer.timerIsRunning = false;
            Destroy(other.gameObject);
            SoundManager.instance.DeathSfx.Play();
           
            //meshAgent.stoppingDistance = 5;
            
            EnemyAnim.SetTrigger("GetGift");
            StartCoroutine(waitForReturn());
            if (GetGifted == false)
            {
                GlobleVariable.instance.Coins += 10;
                enemyGeneration.enemeyCount--;
                if (enemyGeneration.enemeyCount == 0)
                {

                    GameManager.instance.GameOverMethod();
                }
            }
            UIManager.instance.CoinsTxt.text = "Coins: " + GlobleVariable.instance.Coins;
            PlayerPrefs.SetInt("Coin", GlobleVariable.instance.Coins);
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
            //if (enemyGeneration.enemeyCount == 0)
            //{

            //    player.GetComponent<CharacterMovement>().PlayerAnim.SetTrigger("IsWinning");

            //    UIManager.instance.levelCompletePanel.SetActive(true);
            //    SoundManager.instance.WinningSfx.Play();
            //    SoundManager.instance.BgSfx.Stop();
            //    GlobleVariable.instance.LevelCounter++;
            //    PlayerPrefs.SetInt("LevelCounts", GlobleVariable.instance.LevelCounter);
            //    Debug.Log("Levles :" + GlobleVariable.instance.LevelCounter);
            //    GameManager.instance.isPlay = false;
            //}
              Debug.Log("Enemy Number : " + enemyGeneration.enemeyCount);
            //isReturn = true;
            //ReturnInSightRange = true;
        }
    }

    IEnumerator waitingForEnemiesThrow()
    {
        
       // EnemyAnim.SetTrigger("ThrowBall");
        yield return new WaitForSeconds(.5f);
        if (Enemyhealth > 0)
        {
            ///Attack code here
            Rigidbody rb = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            rb.AddForce(transform.up * 2f, ForceMode.Impulse);
            ///End of attack code

            //alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    IEnumerator waitForReturn()
    {

        yield return new WaitForSeconds(1);
        EnemyAnim.SetBool("IsRunning", true);
        meshAgent.stoppingDistance = 5;
        isReturn = true;
        ReturnInSightRange = true;
        yield return new WaitForSeconds(5);
        Destroy(gameObject);

    }
    IEnumerator waitForEnemyDestroy()
    {
        yield return new WaitForSeconds(1);

    }

    private void OnDestroy()
    {
        if (UIManager.instance.GameOverScreen.activeInHierarchy == false && iHaveGift == false)
        {
            GlobleVariable.instance.EnemiesReturnWithoutGift++; // Enemy return without gift
            enemyGeneration.enemeyCount--;
            if (enemyGeneration.enemeyCount == 0)
            {

                GameManager.instance.GameOverMethod();
            }
            UIManager.instance.SadKid.text =""+ GlobleVariable.instance.EnemiesReturnWithoutGift;
        }

      
        //if (UIManager.instance.GameOverScreen == true)
        //{
        //    GameManager.instance.isPlay = false;
        //}

    }

}