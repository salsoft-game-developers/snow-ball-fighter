﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopScript : MonoBehaviour
{

    public Text MyCoinsText;
    //public GameObject Close;
   // public GameObject AdsPanel;
    //public int Skin2;
    //public Text Inventry;
    //public GameObject Lock;
    public GameObject[] Skins;
    public int[] SkinPrice;
    public Text[] ButtonTxt;
    public GameObject[] LockImg;
    public Text[] PriceTxt;
    [SerializeField] Text PriceText;
    public GameObject[] TickImg;
    //public GameObject TickImg_S2;
    private void Start()
    {
        //PlayerPrefs.SetInt("Skin", 1);
        //PlayerPrefs.SetInt("Skin1", 1);
        //PlayerPrefs.SetInt("Skin2", 0);
        //PlayerPrefs.SetInt("Skin3", 0);
      //  AdsPanel.SetActive(false);
        MyCoinsText.text = "" + GlobleVariable.instance.Coins;

        // SetCharacterSelection();

        if (PlayerPrefs.GetInt("Skin1") > 0)
        {

            ButtonTxt[0].text = "Select me";
            TickImg[0].SetActive(false);
        }

        else
        {

            ButtonTxt[0].text = "Select me";
            TickImg[0].SetActive(false);

        }


        if (PlayerPrefs.GetInt("Skin2") > 0)
        {
            LockImg[0].SetActive(false);
            PriceTxt[0].text = null;
            ButtonTxt[1].text = "Select me";
            TickImg[1].SetActive(false);
        }
        else
        {
            LockImg[0].SetActive(true);
            PriceTxt[0].text = "200 Coins";

            ButtonTxt[1].text = "buy";
            TickImg[1].SetActive(false);


        }

        if (PlayerPrefs.GetInt("Skin3", 0) > 0)
        {
            LockImg[1].SetActive(false);
            PriceTxt[1].text = null;
            ButtonTxt[2].text = "Select me";
            TickImg[2].SetActive(false);
        }
        else
        {
            LockImg[1].SetActive(true);
            PriceTxt[1].text = "5 Coins";

            ButtonTxt[2].text = "buy";
            TickImg[2].SetActive(false);
        }




        if (PlayerPrefs.GetInt("Skin4", 0) > 0)
        {
            LockImg[2].SetActive(false);
            PriceTxt[2].text = null;
            ButtonTxt[2].text = "Select me";
            TickImg[3].SetActive(false);
        }
        else
        {
            LockImg[2].SetActive(true);
            PriceTxt[1].text = "300 Coins";

            ButtonTxt[2].text = "buy";
            TickImg[3].SetActive(false);
        }


        ///////////////////////////////////




        if (PlayerPrefs.GetInt("Skin", 1) == 1)
        {

            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[0].SetActive(true); // set character 1 by index
                                      // ButtonTxt[0].text = "Selected"; // set character button text
            TickImg[0].SetActive(true);

        }

        else if (PlayerPrefs.GetInt("Skin", 1) == 2)
        {

            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[1].SetActive(true); // set chacter 2 by index
                                      // ButtonTxt[1].text = "Selected"; // set character button text
            TickImg[1].SetActive(true);
        }

        else if (PlayerPrefs.GetInt("Skin", 1) == 3)
        {

            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[2].SetActive(true); // set chacter 3 by index
            //ButtonTxt[2].text = "Selected"; // set character button text
            TickImg[2].SetActive(true);
        }




        else if (PlayerPrefs.GetInt("Skin", 1) == 4)
        {

            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[3].SetActive(true); // set chacter 3 by index
                                      // ButtonTxt[3].text = "Selected"; // set character button text
            TickImg[3].SetActive(true);
        }





    }
    void SetCharacterSelection(int BtnIndex)
    {
        if (PlayerPrefs.GetInt("Skin1") > 0)
        {
            Debug.Log(PlayerPrefs.GetInt("Skin1") + " Button Skin Value 1");
            // ButtonTxt[0].text = "Select me";
            TickImg[0].SetActive(false);
            // Debug.Log("if Buy");
        }
        else
        {
            Debug.Log(PlayerPrefs.GetInt("Skin1") + " Button Skin Value for buy 1");
            // Debug.Log("else Buy");
            ButtonTxt[0].text = "Select me";
            TickImg[0].SetActive(false);
        }
        if (PlayerPrefs.GetInt("Skin2") > 0)
        {
            Debug.Log(PlayerPrefs.GetInt("Skin2") + " Button Skin Value 2");
            ButtonTxt[1].text = "Select me";
            TickImg[1].SetActive(false);
            // Debug.Log("if Buy");
        }
        else
        {
            Debug.Log(PlayerPrefs.GetInt("Skin2") + " Button Skin Value for buy 2");
            // Debug.Log("else Buy");
            ButtonTxt[1].text = "buy";
            TickImg[1].SetActive(false);


        }
        if (PlayerPrefs.GetInt("Skin3") > 0)
        {
            Debug.Log(PlayerPrefs.GetInt("Skin3") + " Button Skin Value3 ");
            ButtonTxt[2].text = "Select me";
            TickImg[2].SetActive(false);
            // TickImg[2].SetActive(false);
            // Debug.Log("if Buy");
        }
        else
        {
            Debug.Log(PlayerPrefs.GetInt("Skin3") + " Button Skin Value for buy 3");
            // Debug.Log("else Buy");
            ButtonTxt[2].text = "buy";
            TickImg[2].SetActive(false);
            // TickImg[2].SetActive(false);
        }

        if (PlayerPrefs.GetInt("Skin4") > 0)
        {
            Debug.Log(PlayerPrefs.GetInt("Skin4") + " Button Skin Value3 ");
            // ButtonTxt[3].text = "Select me";
            TickImg[3].SetActive(false);
            // TickImg[2].SetActive(false);
            // Debug.Log("if Buy");
        }
        else
        {
            Debug.Log(PlayerPrefs.GetInt("Skin4") + " Button Skin Value for buy 3");
            // Debug.Log("else Buy");
            //ButtonTxt[3].text = "buy";
            TickImg[3].SetActive(false);
            // TickImg[2].SetActive(false);
        }

        // ButtonTxt[BtnIndex].text = "Selected";
        TickImg[BtnIndex].SetActive(true);
    }


    public void Buy1()
    {
        //locked img
        if (PlayerPrefs.GetInt("Skin1", 1) > 0)
        {
            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[0].SetActive(true);// set chacter 1 by index
            PlayerPrefs.SetInt("Skin", 1);
            Debug.Log(PlayerPrefs.GetInt("Skin0") + "skin check");

            SetCharacterSelection(0);

        }
        else
        {

            //if (Globle.Coins >= SkinPrice[1])  /// check coin and unlock
            //{
            //PlayerPrefs.SetInt("Skin1", 1);
            //  Debug.Log(PlayerPrefs.GetInt("Skin2") + "skin value");

            //  Globle.Coins -= SkinPrice[1];

            MyCoinsText.text = "" + GlobleVariable.instance.Coins;
            PlayerPrefs.SetInt("Skin", 1);
            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[0].SetActive(true); // set chacter 1 by index
            SetCharacterSelection(0);
            //}
            //else
            //{
            // Debug.Log("not enough coins");
            //}

        }

    }

    public void Buy2()
    {
        Debug.Log("This is Button 2");
        //locked img
        if (PlayerPrefs.GetInt("Skin2", 0) > 0)
        {
            Debug.Log("Button 2 - Already Unlocked");
            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[1].SetActive(true);   // set chacter 2 by index
            PlayerPrefs.SetInt("Skin", 2);


            SetCharacterSelection(1);


        }
        else
        {
            Debug.Log("Unlocking Button 2");
            if (GlobleVariable.instance.Coins >= SkinPrice[1])  /// check coin and unlock
            {
                Debug.Log("Enough Coins Available for Button 2");
                PlayerPrefs.SetInt("Skin2", 1);
                Debug.Log(PlayerPrefs.GetInt("Skin2") + "skin value");

                GlobleVariable.instance.Coins -= SkinPrice[1];
                // Debug.Log("Coin Minus");
                MyCoinsText.text = "" + GlobleVariable.instance.Coins;
                PlayerPrefs.SetInt("TotalCoins", GlobleVariable.instance.Coins);
                PlayerPrefs.SetInt("Skin", 2);
                LockImg[0].SetActive(false);
                PriceTxt[0].text = null;
                for (int i = 0; i < Skins.Length; i++)
                {
                    Skins[i].SetActive(false);
                }
                Skins[1].SetActive(true);   // set chacter 2 by index


                SetCharacterSelection(1);


            }
            else
            {
              //  AdsPanel.SetActive(true);
                Debug.Log("not enough coins");
            }


        }
    }

    public void Buy3()
    {

        //locked img
        if (PlayerPrefs.GetInt("Skin3", 0) > 0)
        {

            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[2].SetActive(true);   // set chacter 3 by index
            PlayerPrefs.SetInt("Skin", 3);
            Debug.Log("Buy 3 Lock img");

            SetCharacterSelection(2);

        }
        else
        {

            if (GlobleVariable.instance.Coins >= SkinPrice[2])  /// check coin and unlock
            {
                PlayerPrefs.SetInt("Skin3", 1);


                GlobleVariable.instance.Coins -= SkinPrice[2];

                MyCoinsText.text = "" + GlobleVariable.instance.Coins;
                PlayerPrefs.SetInt("TotalCoins", GlobleVariable.instance.Coins);
                PlayerPrefs.SetInt("Skin", 3);
                Debug.Log("Buy 3 is run");
                LockImg[1].SetActive(false);
                PriceTxt[1].text = null;
                for (int i = 0; i < Skins.Length; i++)
                {
                    Skins[i].SetActive(false);
                }
                Skins[2].SetActive(true);    // set chacter 3 by index
                SetCharacterSelection(2);
            }
            else
            {
               // AdsPanel.SetActive(true);
                Debug.Log("not enough coins");
            }
        }
    }



    // Skin 4 button
    public void Buy4()
    {

        //locked img
        if (PlayerPrefs.GetInt("Skin4", 0) > 0)
        {

            for (int i = 0; i < Skins.Length; i++)
            {
                Skins[i].SetActive(false);
            }
            Skins[3].SetActive(true);   // set chacter 3 by index
            PlayerPrefs.SetInt("Skin", 4);
            Debug.Log("Buy 4 Lock img");

            SetCharacterSelection(3);

        }
        else
        {

            if (GlobleVariable.instance.Coins >= SkinPrice[3])  /// check coin and unlock
            {
                PlayerPrefs.SetInt("Skin4", 1);


                GlobleVariable.instance.Coins -= SkinPrice[3];

                MyCoinsText.text = "" + GlobleVariable.instance.Coins;
                PlayerPrefs.SetInt("TotalCoins", GlobleVariable.instance.Coins);
                PlayerPrefs.SetInt("Skin", 4);
                Debug.Log("Buy 4 is run");
                LockImg[2].SetActive(false);
                PriceTxt[2].text = null;
                for (int i = 0; i < Skins.Length; i++)
                {
                    Skins[i].SetActive(false);
                }
                Skins[3].SetActive(true);    // set chacter 3 by index
                SetCharacterSelection(3);
            }
            else
            {
              //  AdsPanel.SetActive(true);
                Debug.Log("not enough coins");
            }
        }
    }


    //// public void TextChange()
    // {
    //     for (int i = 0; i < ButtonTxt.Length; i++)    // change button text
    //     {
    //         //for(int x = 1; x < ButtonTxt.Length; x++)
    //         //{
    //             if (PlayerPrefs.GetInt("Skin" + i) > 0)
    //             {
    //                 ButtonTxt[i].text = "Select me";
    //                 // Debug.Log("if Buy");
    //             }
    //             else
    //             {
    //                 // Debug.Log("else Buy");
    //                 ButtonTxt[i].text = "buy";

    //             }
    //         //}

    //         ButtonTxt[i].text = "Selected";

    //     }
    // }
    public void CloseButton()
    {
       // Close.SetActive(false);
    }
    public void AdsPanelCloseBtn()
    {
        //AdsPanel.SetActive(false);
    }

}
